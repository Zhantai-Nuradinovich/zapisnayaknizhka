﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Labs
{
    class Notebook
    {
        public static List<Person> persons = new List<Person>();
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Сделать запись -> 1");
                Console.WriteLine("Удалить запись -> 2");
                Console.WriteLine("Посмотреть созданные записи -> 3");
                Console.WriteLine("Посмотреть все записи с краткой информацией -> 4");
                Console.WriteLine("Редактировать запись -> 5");
                Console.WriteLine("Выйти -> любое");

                int a = int.TryParse(Console.ReadLine(), out int dd)? dd : 10;

                switch (a)
                {
                    case 1:
                        if (CreateNote())
                        {
                            Console.WriteLine("++++1 Человек \n нажмите на любую клавишу");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    case 2:
                        if (DeleteNote())
                        {
                            Console.WriteLine("----1 Человек \n нажмите на любую клавишу");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else
                        {
                            Console.WriteLine("Не найден(( \n нажмите на любую клавишу");
                            Console.ReadKey();
                            Console.Clear();
                        }

                        break;
                    case 3:
                        ReadNote();
                        break;
                    case 4:
                        ShowAllNotes();
                        break;
                    case 5:
                        Console.WriteLine("Посмотрите и запомните ID");
                        ShowAllNotes();
                        Console.WriteLine("Введите ID пользователя для редактирования");
                        int id = int.TryParse(Console.ReadLine(), out int d) ? d : -1;
                        if (id == -1)
                        {
                            break;
                        }
                        else
                        {
                            foreach (Person p in persons)
                            {
                                if (p.Id == id)
                                {
                                    EditNote(p);
                                    Console.Clear();
                                    break;
                                }
                            }
                        }
                        
                        break;
                    default:
                        Console.WriteLine("Нет такого! Хотите выйти?? [да, нет]");
                        switch (Console.ReadLine())
                        {
                            case "да":
                                Environment.Exit(0);
                                break;
                            case "нет":
                                break;
                            default:
                                Environment.Exit(0);
                                break;
                        }
                        break;
                }
            }
        }

        static bool CreateNote()
        {
            Console.Clear();
            Console.WriteLine("Введите данные");

            Console.WriteLine("Имя : ");
            string firstName = Console.ReadLine();
            while (firstName == null)
            {
                Console.WriteLine("Это поле обязательно");
                firstName = Console.ReadLine();
            }

            Console.WriteLine("Фамилия : ");
            string lastName = Console.ReadLine();
            while (lastName == null)
            {
                Console.WriteLine("Это поле обязательно");
                lastName = Console.ReadLine();
            }

            Console.WriteLine("Отчество (необязательно) : ");
            string neobyazName = Console.ReadLine();

            Console.WriteLine("Номер телефона (только цифры) : ");

            int phoneNumber = int.TryParse(Console.ReadLine(), out int a)? a : 0; 
            while(phoneNumber == 0)
            {
                Console.WriteLine("Номер телефона не может быть таким : ");
                phoneNumber = int.TryParse(Console.ReadLine(), out int b) ? b : 0;
            }

            Console.WriteLine("Страна : ");
            string country = Console.ReadLine();
            while (country == null)
            {
                Console.WriteLine("Это поле обязательно");
                country = Console.ReadLine();
            }

            Console.WriteLine("Дата рождения (необязательно) : ");
            DateTime dateOfBirth = DateTime.TryParse(Console.ReadLine(), out DateTime c)? c : new DateTime(01, 01, 0001);

            Console.WriteLine("Организация (необязательно) : ");
            string organization = Console.ReadLine();

            Console.WriteLine("Должность (необязательно) : ");
            string dolzhnost = Console.ReadLine();

            Console.WriteLine("Заметка (необязательно) : ");
            string notes = Console.ReadLine();

            persons.Add(new Person(firstName, lastName, neobyazName, phoneNumber, country, dateOfBirth, organization, dolzhnost, notes));
            Console.Clear();
            return true;
        }
        static void EditNote(Person person)
        {
            Console.Clear();
            Console.WriteLine("Выберите, что хотите исправить: \n 1 - имя,  2 - фамилия, 3 - отчество, \n ");
            Console.WriteLine("4 - телефон,  5 - дата рождения, 6 - страна, \n 7 - организация, 8 - должность, 9 - примечания");
            int choise = int.TryParse(Console.ReadLine(), out int a) ? a : 0;
            Console.WriteLine("Вводите: ");
            switch (a) {
                case 1:
                    person.FirstName = Console.ReadLine();
                    break;
                case 2:
                    person.LastName = Console.ReadLine();
                    break;
                case 3:
                    person.NeobyazName = Console.ReadLine();
                    break;
                case 4:
                    person.PhoneNumber = int.TryParse(Console.ReadLine(), out int sss) ? sss : 0;
                    while (person.PhoneNumber == 0)
                    {
                        Console.WriteLine("Номер телефона не может быть таким : ");
                        person.PhoneNumber = int.TryParse(Console.ReadLine(), out int b) ? b : 0;
                    }
                    break;
                case 5:
                    person.DateOfBirth = DateTime.TryParse(Console.ReadLine(), out DateTime aaaa) ? aaaa : new DateTime();
                    break;
                case 6:
                    person.Country = Console.ReadLine();
                    break;
                case 7:
                    person.Organization = Console.ReadLine();
                    break;
                case 8:
                    person.Dolzhnost = Console.ReadLine();
                    break;
                case 9:
                    person.Notes = Console.ReadLine();
                    break;
                default:
                    Console.WriteLine("Такого нет!");
                    Console.ReadKey();
                    Console.Clear();
                    break;
            }
        }
        static void ReadNote()
        {
            Console.Clear();

            Console.WriteLine("Фамилия   |   Имя   |   Отчество   |   Номер   |   Страна   |   Дата рождения   |   Организация   |   Должность   |   Заметки   ");
            foreach(Person p in persons)
            {
                Console.WriteLine(p.LastName + " | " + p.FirstName + " | " + p.NeobyazName + " | " +
                    p.PhoneNumber + " | " + p.Country + " | " + p.DateOfBirth.ToShortDateString() + " | " + p.Organization + " | " + p.Dolzhnost + " | " + p.Notes);
            }

            Console.ReadKey();
            Console.Clear();
        }
        static bool DeleteNote()
        {
            Console.Clear();
            Console.WriteLine("Введите имя");
            string firstName = Console.ReadLine();
            Console.WriteLine("Введите фамилию");
            string lastName = Console.ReadLine();
            List<Person> search = new List<Person>();
            foreach(Person p in persons)
            {
                if(p.FirstName == firstName && p.LastName == lastName)
                {
                    search.Add(p);
                }
            }
            if(search.Count == 0)
            {
                Console.Clear();
                return false;
            }
            Console.WriteLine("Введите id того пользователя, которого хотите удалить");
            foreach(Person p in search)
            {
                Console.WriteLine(p);
            }
            int del = int.Parse(Console.ReadLine());
            for (int i = 0; i < persons.Count; i++)
            {
                if(persons[i].Id == del)
                {
                    persons.Remove(persons[i]);
                    Console.Clear();
                    return true;
                }
            }
            return false;
        }

        static void ShowAllNotes()
        {
            Console.Clear();
            foreach(Person p in persons)
            {
                Console.WriteLine(p);
            }
            Console.ReadKey();
            Console.Clear();
        }
    }

    //Entity Person
    public class Person
    {
        private long id = 0;
        public long Id 
        {
            get { return this.id; }
            private set { this.id = value;  }
        }
        private static long next = 0;

        private string firstName;
        public string FirstName
        {
            get { return this.firstName; }
            set { this.firstName = value; }
        }

        private string lastName;
        public string LastName
        {
            get { return this.lastName; }
            set { this.lastName = value; }
        }

        private string neobyazName;
        public string NeobyazName
        {
            get { return this.neobyazName; }
            set { this.neobyazName = value; }
        }

        private int phoneNumber;
        public int PhoneNumber
        {
            get { return this.phoneNumber; }
            set { this.phoneNumber = value; }
        }

        private string country;
        public string Country
        {
            get { return this.country; }
            set { this.country = value; }
        }

        private DateTime dateOfBirth;
        public DateTime DateOfBirth
        {
            get { return this.dateOfBirth; }
            set { this.dateOfBirth = value; }
        }

        private string organization;
        public string Organization
        {
            get { return this.organization; }
            set { this.organization = value; }
        }

        private string dolzhnost;
        public string Dolzhnost
        {
            get { return this.dolzhnost; }
            set { this.dolzhnost = value; }
        }

        

        private string notes;
        public string Notes
        {
            get { return this.notes; }
            set { this.notes = value; }
        }

        public Person()
        {
            this.id = next;
            next++;
        }

        public Person(
                        string firstName, string lastName, string neobyazName,
                        int phoneNumber, string country, DateTime dateOfBirth, string organization,
                        string dolzhnost, string notes
                     )
        {
            this.id = next;
            next++;

            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNumber = phoneNumber;
            this.country = country;

            this.neobyazName = neobyazName;
            this.dateOfBirth = dateOfBirth;
            this.organization = organization;
            this.dolzhnost = dolzhnost;
            this.notes = notes;
        }

        public override string ToString()
        {
            return id+ ". " + LastName + ", " + FirstName + ", " + PhoneNumber;
        }
    }

}
